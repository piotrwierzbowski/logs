## Assignment task
Based on the description [in the file](Coding Assignment for Java.pdf)

## Running the program: 
### Requirements:
Java 8 JDK installed

Gradle

### Execution:
Download the repository from git

Execute:
`./gradlew clean build run --args="X:\path-to-your-file"`

Verify the results with HSQLDB client in data\mydb


## Developing the program:
You will need some Spring Boot knowledge and Java
You may need to install Lombok in your IDE first, see: https://www.baeldung.com/lombok-ide

## Configuration
The alert treshold parameter can be changed, simply configure *pl.piotrwierzbowski.logsProcessorService.alertTreshold* parameter, for example via application.properties file or any other method suitable in Spring framework

### Testing:
Execute:
`./gradlew clean build test`

## Unfinished:
Add multithreading, tried with *@Async* but eventually got to the issue described here: https://stackoverflow.com/questions/46792838/spring-command-line-app-hangs-after-async-method-calls-complete/46794654 
Spring boot was not exiting the application when done the processing, fixing would extend the total completion of the task way beyond planned 2 hours as the description would require some application methods rewrite.