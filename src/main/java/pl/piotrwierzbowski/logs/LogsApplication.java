package pl.piotrwierzbowski.logs;

import java.io.File;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.piotrwierzbowski.logs.service.LogsProcessorService;

@RequiredArgsConstructor
@Log4j2
@SpringBootApplication
public class LogsApplication implements CommandLineRunner {

	private final LogsProcessorService logsProcessorService;
	
	public static void main(String[] args) {
		SpringApplication.run(LogsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if (args.length != 1 || !logsProcessorService.isFileInPathValid(args[0])) {
			log.error("Expected 1 argument with a readble logs file provided to the program");
		} else {
			File file = new File(args[0]);
			
			log.info("Processing the file {} started", file);
			logsProcessorService.processFile(file);
			log.info("Processing the file {} finished", file);
		}
	}

}
