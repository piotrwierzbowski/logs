package pl.piotrwierzbowski.logs.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.piotrwierzbowski.logs.domain.LoggedEvent;

@Repository
public interface LoggedEventRepository extends JpaRepository<LoggedEvent, String> {
	
}
