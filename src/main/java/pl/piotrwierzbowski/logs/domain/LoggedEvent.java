package pl.piotrwierzbowski.logs.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoggedEvent {
	
	@Id
	private String id;
	private long duration;
	private String type;
	private String host;
	private boolean alert;
}
