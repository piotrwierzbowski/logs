package pl.piotrwierzbowski.logs.model;

public enum LogState {
	STARTED,
	FINISHED
}
