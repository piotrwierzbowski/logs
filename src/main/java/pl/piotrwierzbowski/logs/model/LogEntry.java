package pl.piotrwierzbowski.logs.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LogEntry {

	private String id;
	private LogState state;
	private long timestamp;
	private String type;
	private String host;
}
