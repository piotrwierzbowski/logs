package pl.piotrwierzbowski.logs.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.piotrwierzbowski.logs.dao.LoggedEventRepository;
import pl.piotrwierzbowski.logs.domain.LoggedEvent;

@Log4j2
@Transactional
@Service
@RequiredArgsConstructor
public class LoggedEventService {

	private final LoggedEventRepository loggedEventRepository; 
	
	public void saveEvent(LoggedEvent loggedEvent) {
		loggedEventRepository.save(loggedEvent);
		log.debug("Saved {}", loggedEvent);
	}
}
