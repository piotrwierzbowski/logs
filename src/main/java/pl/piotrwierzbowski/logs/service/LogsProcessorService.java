package pl.piotrwierzbowski.logs.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.piotrwierzbowski.logs.domain.LoggedEvent;
import pl.piotrwierzbowski.logs.model.LogEntry;
import pl.piotrwierzbowski.logs.model.LogState;

@RequiredArgsConstructor
@Log4j2
@Service
public class LogsProcessorService {

	@Value("${pl.piotrwierzbowski.logsProcessorService.alertTreshold}")
	private long alertTreshold;
	
	private final ObjectMapper objectMapper;
	
	private final LoggedEventService loggedEventService; 
	
	public void processFile(File file) throws IOException {
		// hold a cache for unfinished entries as id -> value map
		Map<String, LogEntry> logsCache = new HashMap<>();
		
		// using iterator here as loading it plainly to a stream could result in OOM
		try (LineIterator it = FileUtils.lineIterator(file)) {
			it.forEachRemaining(line -> consumeLine(line, logsCache));
		}
	}
	
	protected void consumeLine(String line, Map<String, LogEntry> logsCache) {
		Optional<LogEntry> logEntry = parseLine(line);
		if (logEntry.isPresent()) {
			processEntry(logEntry.get(), logsCache);
		}
	}
	
	protected Optional<LoggedEvent> processEntry(LogEntry newLogEntry, Map<String, LogEntry> logsCache) {
		if (logsCache.containsKey(newLogEntry.getId())) {
			LogEntry oldLogEntry = logsCache.get(newLogEntry.getId());
			Optional<LoggedEvent> loggedEntry = mergeEntriesToLoggedEvent(newLogEntry, oldLogEntry);
			
			if (loggedEntry.isPresent()) {
				logsCache.remove(newLogEntry.getId());
				loggedEventService.saveEvent(loggedEntry.get());
				log.debug("Correctly processed entry with id '{}'", newLogEntry.getId());
			}
			
			return loggedEntry;
		} else {
			logsCache.put(newLogEntry.getId(), newLogEntry);
			return Optional.empty();
		}
	}

	protected Optional<LoggedEvent> mergeEntriesToLoggedEvent(LogEntry newLogEntry, LogEntry oldLogEntry) {
		Optional<LogEntry> startingEventOp = Stream.of(newLogEntry, oldLogEntry)
			.filter(entry -> LogState.STARTED.equals(entry.getState()))
			.findFirst();
		
		Optional<LogEntry> finishingEventOp = Stream.of(newLogEntry, oldLogEntry)
				.filter(entry -> LogState.FINISHED.equals(entry.getState()))
				.findFirst();
		
		if (!startingEventOp.isPresent() || !finishingEventOp.isPresent()) {
			log.warn("Found event with id '{}' without a start to finish relation", newLogEntry.getId());
			return Optional.empty();
		}
		
		LogEntry startingEvent = startingEventOp.get();
		LogEntry finishingEvent = finishingEventOp.get();
		
		long duration = finishingEvent.getTimestamp() - startingEvent.getTimestamp();
		log.debug("Event id '{}' has {}ms duration", startingEvent.getId(), duration);
		
		LoggedEvent loggedEvent = LoggedEvent.builder()
			.id(startingEvent.getId())
			.duration(duration)
			.type(Optional.ofNullable(startingEvent.getType()).orElseGet(() -> finishingEvent.getType()))
			.host(Optional.ofNullable(startingEvent.getHost()).orElseGet(() -> finishingEvent.getHost()))
			.alert(duration > alertTreshold)
			.build();
		
		return Optional.of(loggedEvent);
	}

	protected Optional<LogEntry> parseLine(String line) {
		try {
			log.debug("Parsing line '{}'", line);
			return Optional.of(objectMapper.readValue(line, LogEntry.class));
		} catch (JsonProcessingException e) {
			log.warn("Failed to parse line '{}' check your input file", line);
			log.warn(e);
			return Optional.empty();
		}
	}
	
	public boolean isFileInPathValid(String filePath) {
		Path path = Paths.get(filePath);
		return Files.exists(path) && Files.isReadable(path);
	}
}
