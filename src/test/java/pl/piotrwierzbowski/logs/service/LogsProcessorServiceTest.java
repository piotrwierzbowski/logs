package pl.piotrwierzbowski.logs.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.piotrwierzbowski.logs.domain.LoggedEvent;
import pl.piotrwierzbowski.logs.model.LogEntry;
import pl.piotrwierzbowski.logs.model.LogState;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { LogsProcessorService.class })
@TestPropertySource(properties="pl.piotrwierzbowski.logsProcessorService.alertTreshold=2")
class LogsProcessorServiceTest {

	private static final String DATA_LOGFILE_TXT = "data/logfile.txt";
	
	@MockBean
	private LoggedEventService loggedEventService;
	
	@SpyBean
	private ObjectMapper objectMapper;
	
	@Autowired
	private LogsProcessorService logsProcessorService;
	
	@Test
	void testProcessFile() throws IOException {
		// given
		File testFile = getFileFromClassPath(DATA_LOGFILE_TXT);
		
		// when
		logsProcessorService.processFile(testFile);
		
		// then
		verify(loggedEventService, times(3)).saveEvent(any());
	}
	
	@Test
	void testProcessEntryCachesLogs() {
		// given
		Map<String, LogEntry> logsCache = new HashMap<>();
		
		String id1 = "1";
		LogEntry entry1 = LogEntry.builder()
				.id(id1)
				.state(LogState.STARTED)
				.timestamp(1)
				.build();
		
		String id2 = "2";
		LogEntry entry2 = LogEntry.builder()
				.id(id2)
				.state(LogState.FINISHED)
				.timestamp(2)
				.build();
		
		// when
		logsProcessorService.processEntry(entry1, logsCache);
		logsProcessorService.processEntry(entry2, logsCache);
		
		// then
		assertThat(logsCache).containsValues(entry1, entry2);
	}

	@Test
	void testProcessEntryCleansUpCache() {
		// given
		Map<String, LogEntry> logsCache = new HashMap<>();
		
		String id = "1";
		LogEntry entry1 = LogEntry.builder()
				.id(id)
				.state(LogState.STARTED)
				.timestamp(1)
				.build();
		
		LogEntry entry2 = LogEntry.builder()
				.id(id)
				.state(LogState.FINISHED)
				.timestamp(2)
				.build();
		
		Optional<LoggedEvent> expectedResult = Optional.of(LoggedEvent.builder()
				.id(id)
				.duration(1)
				.alert(false)
				.build());
		
		logsCache.put(id, entry1);
		
		// when
		Optional<LoggedEvent> loggedEvent = logsProcessorService.processEntry(entry2, logsCache);
		
		// then
		assertThat(loggedEvent).isEqualTo(expectedResult);
		assertThat(logsCache).isEmpty();
	}

	@Test
	void testMergeEntriesToLoggedEvent() {
		// given
		String id = "1";
		LogEntry entry1 = LogEntry.builder()
				.id(id)
				.state(LogState.STARTED)
				.timestamp(1)
				.build();
		
		LogEntry entry2 = LogEntry.builder()
				.id(id)
				.state(LogState.FINISHED)
				.timestamp(2)
				.build();
		
		Optional<LoggedEvent> expectedResult = Optional.of(LoggedEvent.builder()
				.id(id)
				.duration(1)
				.alert(false)
				.build());
		
		// when
		Optional<LoggedEvent> loggedEvent = logsProcessorService.mergeEntriesToLoggedEvent(entry1, entry2);
		Optional<LoggedEvent> loggedEventReversed = logsProcessorService.mergeEntriesToLoggedEvent(entry2, entry1);
		
		// then
		assertThat(loggedEvent).isEqualTo(expectedResult);
		assertThat(loggedEventReversed).isEqualTo(expectedResult);
	}
	
	@Test
	void testMergeEntriesToLoggedEventDuplicate() {
		// given
		String id = "1";
		LogEntry entry1 = LogEntry.builder()
				.id(id)
				.state(LogState.STARTED)
				.timestamp(1)
				.build();
		
		LogEntry entry2 = LogEntry.builder()
				.id(id)
				.state(LogState.STARTED)
				.timestamp(2)
				.build();
		
		// when
		Optional<LoggedEvent> loggedEvent = logsProcessorService.mergeEntriesToLoggedEvent(entry1, entry2);
		Optional<LoggedEvent> loggedEventReversed = logsProcessorService.mergeEntriesToLoggedEvent(entry2, entry1);
		
		// then
		assertThat(loggedEvent).isEmpty();
		assertThat(loggedEventReversed).isEmpty();
	}

	@Test
	void testParseLine() {
		// given
		String line = "{\"id\": \"scsmbstgra\", \"state\": \"FINISHED\", \"type\": \"APPLICATION_LOG\", \"host\": 12345, \"timestamp\": 1491377495217}";
		LogEntry logEntry = LogEntry.builder()
				.id("scsmbstgra")
				.state(LogState.FINISHED)
				.type("APPLICATION_LOG")
				.host("12345")
				.timestamp(1491377495217L)
				.build();
		
		// when
		Optional<LogEntry> parsedEntry = logsProcessorService.parseLine(line);
		
		// then
		assertThat(parsedEntry).isNotEmpty();
		assertThat(parsedEntry.get()).isEqualTo(logEntry);
	}
	
	@Test
	void testParseLineFail() {
		// given
		String line = "{that is a non-parsable json object}";
		
		// when
		Optional<LogEntry> parsedEntry = logsProcessorService.parseLine(line);
		
		// then
		assertThat(parsedEntry).isEmpty();
	}

	@Test
	void testIsFileInPathValid() {
		// prepare
		String absolutePath = getFileFromClassPath(DATA_LOGFILE_TXT).getAbsolutePath();
		
		// verify
		assertThat(logsProcessorService.isFileInPathValid(absolutePath)).isTrue();
	}
	
	@Test
	void testIsFileInPathValidFail() {
		// prepare
		String absolutePath = getFileFromClassPath(DATA_LOGFILE_TXT).getAbsolutePath() + "not-existing-path-to-file";
		
		// verify
		assertThat(logsProcessorService.isFileInPathValid(absolutePath)).isFalse();
	}

	private File getFileFromClassPath(String fileRelativePath) {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileRelativePath).getFile());
		return file;
	}
	
}
