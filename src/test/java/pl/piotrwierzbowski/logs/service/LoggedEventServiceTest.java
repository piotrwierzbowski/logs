package pl.piotrwierzbowski.logs.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import pl.piotrwierzbowski.logs.dao.LoggedEventRepository;
import pl.piotrwierzbowski.logs.domain.LoggedEvent;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class LoggedEventServiceTest {

	@Autowired
	private LoggedEventService loggedEventService;
	
	@Autowired
	private LoggedEventRepository loggedEventRepository;
	
	@Test
	void testSaveEvent() throws InterruptedException {
		// given
		String id = UUID.randomUUID().toString();
		
		LoggedEvent event = LoggedEvent.builder()
			.alert(true)
			.duration(1)
			.host("host")
			.type("type")
			.id(id)
			.build();
		
		// when
		loggedEventService.saveEvent(event);
		
		// then
		Optional<LoggedEvent> savedEvent = loggedEventRepository.findById(id);
		assertThat(savedEvent).isNotEmpty();
		assertThat(savedEvent.get()).isEqualTo(event);
	}
}